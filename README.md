# PHP-Supercronic

A container with PHP-FPM and some modules like Redis installed.

This container is meant to be used as a task executor with Cron. But we're using Supercronic that is more verbose and support features designed for the container world.

## How to use?

Map your project to the container and also map a crontab file. Run it and you're done.

Sample usage:

```yml
## This is an example file. Use it straight in your project!
version: "3.6"
services:
    cron:
      image: registry.gitlab.com/andersonpem/php-cron:1.0
      restart: always
      volumes:
        # Map your project's document root.
        - ./:/var/www/html
        # Give it a crontab file so it runs when the container starts
        - ./cron:/etc/crontabs/root
```

That's it :)